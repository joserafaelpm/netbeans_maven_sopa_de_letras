/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import java.awt.AWTException;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author w
 */
public class PantallazoFactory {

    public static void capturarPantalla(javax.swing.JPanel panel) throws
            AWTException, IOException, InterruptedException {
        Thread.sleep(1000);
        BufferedImage captura = new BufferedImage(panel.getWidth(), panel.getHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics2D g = captura.createGraphics();
        panel.paint(g);
        ImageIO.write(captura, "jpg", new File("SopaDeLetras.jpg"));
    }
}
