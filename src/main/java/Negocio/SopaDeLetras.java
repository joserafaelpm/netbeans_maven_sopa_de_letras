package Negocio;

import Vista.Matriz_Excel_Ejemplo;

/**
 * Write a description of class SopaDeLetras here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class SopaDeLetras {

    // instance variables - replace the example below with your own
    private char sopas[][];

    //listaS de coordenadas
    /**
     * Constructor for objects of class SopaDeLetras
     */
    public SopaDeLetras() {

    }

    public SopaDeLetras pruebaLeerSopaLetras(String p) throws Exception {
        Matriz_Excel_Ejemplo mm = new Matriz_Excel_Ejemplo(p);
        String soparesultante = mm.leerExcel();
        SopaDeLetras sopa = new SopaDeLetras(soparesultante);
        return sopa;
    }

    public SopaDeLetras(String palabras) throws Exception {
        if (palabras == null || palabras.isEmpty()) {
            throw new CustomException("Error no se puede crear la matriz de char para la sopa de letras");
        }

        //Crear la matriz con las correspondientes filas:
        String palabras2[] = palabras.split(",");
        this.sopas = new char[palabras2.length][];
        int i = 0;
        for (String palabraX : palabras2) {
            //Creando las columnas de la fila i
            this.sopas[i] = new char[palabraX.length()];
            pasar(palabraX, this.sopas[i]);
            i++;

        }

    }

    private void pasar(String palabra, char fila[]) {

        for (int j = 0; j < palabra.length(); j++) {
            fila[j] = palabra.charAt(j);
        }
    }

    public String toString() {
        String msg = "";
        for (int i = 0; i < this.sopas.length; i++) {
            for (int j = 0; j < this.sopas[i].length; j++) {
                msg += this.sopas[i][j] + "\t";
            }

            msg += "\n";

        }
        return msg;
    }

    public String toString2() {
        String msg = "";
        for (char filas[] : this.sopas) {
            for (char dato : filas) {
                msg += dato + "\t";
            }

            msg += "\n";

        }
        return msg;
    }

    public boolean esCuadrada() {
        if (this.sopas.length == this.sopas[0].length && !esDispersa()) {
            return true;
        }
        return false;
    }

    public boolean esDispersa() {
        int tamaño = this.sopas[0].length;
        for (int i = 1; i < this.sopas.length; i++) {
            if (this.sopas[i].length != tamaño) {
                return true;
            }
        }
        return false;
    }

    public boolean esRectangular() {
        if (!esDispersa() && this.sopas.length != this.sopas[0].length) {
            return true;
        }
        return false;
    }

    public ListaS<ListaS<Coordenada>> encontrarPalabra(String palabraBuscar) {
        ListaS<ListaS<Coordenada>> l1 = this.getDiagonalCentro(palabraBuscar);
        ListaS<ListaS<Coordenada>> l2 = this.getHorizontalVertical(palabraBuscar);
        l1.unirList(l2);
        return l1;
    }

    public ListaS<ListaS<Coordenada>> getHorizontalVertical(String palabraBuscar) {
        ListaS<ListaS<Coordenada>> coordenadasTotales = new ListaS<>();
        int i = 0, j = 0;
        for (char x[] : this.sopas) {
            for (char y : x) {
                if (y == palabraBuscar.charAt(0)) {
                    ListaS[] coordenadas = {this.horizontalDerecha(i, j, palabraBuscar), this.horizontalIzquierda(i, j, palabraBuscar),
                        this.verticalInferior(i, j, palabraBuscar), this.verticalSuperior(i, j, palabraBuscar)};
                    for (ListaS<Coordenada> coordenada : coordenadas) {
                        if (coordenada != null) {
                            coordenadasTotales.insertarAlInicio(coordenada);
                        }
                    }
                }
                j++;
            }
            j = 0;
            i++;
        }

        return coordenadasTotales;
    }

    //es necesario que retorne coordenadas
    //o retorna string 
    //pero tenga en cuetna que estos valores los va usar para crear el pdf
    public ListaS<Coordenada> horizontalIzquierda(int i, int j, String palabraBuscar) {
        ListaS<Coordenada> coordenadas = new ListaS<>();
        int k = 0;
        while (j < this.sopas[0].length) {
            if (this.sopas[i][j] == palabraBuscar.charAt(k)) {
                coordenadas.insertarAlInicio(new Coordenada(i, j));
                k++;
                j++;
                if (k == palabraBuscar.length()) {
                    return coordenadas;
                }
            } else {
                return null;
            }
        }
        return null;
    }

    public ListaS<Coordenada> horizontalDerecha(int i, int j, String palabraBuscar) {
        ListaS<Coordenada> coordenadas = new ListaS<>();
        int k = 0;
        while (j >= 0) {
            if (this.sopas[i][j] == palabraBuscar.charAt(k)) {
                coordenadas.insertarAlInicio(new Coordenada(i, j));
                k++;
                j--;
                if (k == palabraBuscar.length()) {
                    return coordenadas;
                }
            } else {
                return null;
            }
        }
        return null;
    }

    public ListaS<Coordenada> verticalSuperior(int i, int j, String palabraBuscar) {
        ListaS<Coordenada> coordenadas = new ListaS<>();
        int k = 0;
        while (i < this.sopas.length) {
            if (this.sopas[i][j] == palabraBuscar.charAt(k)) {
                coordenadas.insertarAlInicio(new Coordenada(i, j));
                k++;
                i++;
                if (k == palabraBuscar.length()) {
                    return coordenadas;
                }
            } else {
                return null;
            }
        }
        return null;
    }

    public ListaS<Coordenada> verticalInferior(int i, int j, String palabraBuscar) {
        ListaS<Coordenada> coordenadas = new ListaS<>();
        int k = 0;
        while (i >= 0) {
            if (this.sopas[i][j] == palabraBuscar.charAt(k)) {
                coordenadas.insertarAlInicio(new Coordenada(i, j));
                k++;
                i--;
                if (k == palabraBuscar.length()) {
                    return coordenadas;
                }
            } else {
                return null;
            }
        }
        return null;
    }

//    public Integer getNumDiagonales(String palabraBuscar){
//        int contador=0;
//        for (int i = 0; i < this.sopas.length; i++) {
//            for (int j = 0; j < this.sopas[0].length; j++) {
//                if (this.sopas[i][j] == palabraBuscar.charAt(0)) {
//                    if (this.diagInferiorDerecha(i, j, palabraBuscar)) {
//                        contador++;
//                    }
//                    if (this.diagInferiorIzquierda(i, j, palabraBuscar)) {
//                        contador++;
//                    }
//                    if (this.diagSuperiorDerecha(i, j, palabraBuscar)) {
//                        contador++;
//                    }
//                    if (this.diagSuperiorIzquierda(i, j, palabraBuscar)) {
//                        contador++;    
//                    }
//                }
//            }
//        }
//        return contador;
//    }
    public ListaS<ListaS<Coordenada>> getDiagonalCentro(String palabraBuscar) {
        ListaS<ListaS<Coordenada>> coordenadasTotales = new ListaS<>();
        int i = 0, j = 0;
        for (char x[] : this.sopas) {
            for (char y : x) {
                if (y == palabraBuscar.charAt(0)) {
                    ListaS[] coordenadas = {this.diagSuperiorIzquierda(i, j, palabraBuscar), this.diagSuperiorDerecha(i, j, palabraBuscar),
                        this.diagInferiorDerecha(i, j, palabraBuscar), this.diagInferiorIzquierda(i, j, palabraBuscar)};
                    for (ListaS<Coordenada> coordenada : coordenadas) {
                        if (coordenada != null) {
                            coordenadasTotales.insertarAlInicio(coordenada);
                        }
                    }
                }
                j++;
            }
            j = 0;
            i++;
        }
        return coordenadasTotales;
    }

    private ListaS<Coordenada> diagSuperiorIzquierda(int i, int j, String palabraBuscar) {
        ListaS<Coordenada> coordenadas = new ListaS<>();
        int d = 0, k = 0;
        while (i >= 0 && j >= 0) {
            if (this.sopas[i][j] == palabraBuscar.charAt(k)) {
                d++;
                k++;
                coordenadas.insertarAlInicio(new Coordenada(i, j));
                if (d == palabraBuscar.length()) {
                    return coordenadas;
                }
            } else {
                return null;
            }
            i--;
            j--;
        }
        return null;
    }

    private ListaS<Coordenada> diagSuperiorDerecha(int i, int j, String palabraBuscar) {
        ListaS<Coordenada> coordenadas = new ListaS<>();
        int d = 0, k = 0;
        while (i >= 0 && j < sopas[0].length) {
            if (this.sopas[i][j] == palabraBuscar.charAt(k)) {
                d++;
                k++;
                coordenadas.insertarAlInicio(new Coordenada(i, j));
                if (d == palabraBuscar.length()) {
                    return coordenadas;
                }
            } else {
                return null;
            }
            i--;
            j++;
        }
        return null;
    }

    private ListaS<Coordenada> diagInferiorIzquierda(int i, int j, String palabraBuscar) {
        ListaS<Coordenada> coordenadas = new ListaS<>();
        int d = 0, k = 0;
        while (i < this.sopas.length && j >= 0) {
            if (this.sopas[i][j] == palabraBuscar.charAt(k)) {
                d++;
                k++;
                coordenadas.insertarAlInicio(new Coordenada(i, j));
                if (d == palabraBuscar.length()) {
                    return coordenadas;
                }
            } else {
                return null;
            }
            i++;
            j--;
        }
        return null;
    }

    private ListaS<Coordenada> diagInferiorDerecha(int i, int j, String palabraBuscar) {
        ListaS<Coordenada> coordenadas = new ListaS<>();
        int d = 0, k = 0;
        while (i < this.sopas.length && j < this.sopas[0].length) {
            if (this.sopas[i][j] == palabraBuscar.charAt(k)) {
                d++;
                k++;
                coordenadas.insertarAlInicio(new Coordenada(i, j));
                if (d == palabraBuscar.length()) {
                    return coordenadas;
                }
            } else {
                return null;
            }
            i++;
            j++;
        }
        return null;
    }

    /*
        retorna cuantas veces esta la palabra en la matriz
     */
    public int getContar(String palabraBuscar) {
        return this.encontrarPalabra(palabraBuscar).getTamanio();
    }

    /*
        debe ser cuadrada sopas
     */
//    public char[] getDiagonalPrincipal() throws Exception {
//        char xd[] = new char[this.sopas.length];
//        if (esCuadrada()) {
//            for (int i = 0; i < this.sopas.length; i++) {
//                for (int j = 0; j < this.sopas[0].length; j++) {
//                    if (i == j) {
//                        xd[i] = sopas[i][j];
//                    }
//
//                }
//            }
//        } else {
//            throw new Exception("La matriz no es Cuadrada");
//        }
//        return xd;
//    }
    //Start GetterSetterExtension Source Code
    /**
     * GET Method Propertie sopas
     */
    public char[][] getSopas() {
        return this.sopas;
    }//end method getSopas

    //End GetterSetterExtension Source Code
//!
}
